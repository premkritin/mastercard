#!/bin/bash
#Function to populate Dockerfile
DOCKER_IMG_NAME=helloworld

function populate_docker_file()
{

cat << EOF > hello-world.sh
#!/bin/bash
#echo '<html><head><title>HTTP Hello World</title></head><body><h1>Application running on Docker Instance: '\$(hostname)'</h1></body></html' > /www/index.html
echo 'Application now running : v$DOCKER_IMG_TAG_VERSION' > /www/index.html
python -m SimpleHTTPServer 80
EOF
chmod 755 hello-world.sh

cat << EOF > Dockerfile
FROM ubuntu:latest
RUN apt-get update && \
apt-get upgrade -y && \
DEBIAN_FRONTEND=noninteractive apt-get install -y \
python &&\
apt-get clean && \
rm -rf /var/lib/apt/lists/*
RUN mkdir /www  
WORKDIR /www  
COPY hello-world.sh /www
RUN chmod 755 hello-world.sh
EXPOSE 80
ENTRYPOINT ["./hello-world.sh"]
EOF
    

}

function start_image_registry()
{
    printf "Creating Private Docker registry..\n"
    docker info
    docker pull registry:2
    docker run -it -d -p 5000:5000 -v /var/lib/registry:/var/lib/registry --restart=always --name registry registry:2
    docker ps
}

#Function to Build Docker Image
function build_docker_image()
{
    populate_docker_file
    #eval $(minikube docker-env)
    printf "Creating docker image: $DOCKER_IMG_NAME:v$DOCKER_IMG_TAG_VERSION...\n"
    docker build -t 127.0.0.1:5000/$DOCKER_IMG_NAME:v$DOCKER_IMG_TAG_VERSION .
    printf "Pushing docker image: $DOCKER_IMG_NAME:v$DOCKER_IMG_TAG_VERSION to private docker registry...\n"
    docker push 127.0.0.1:5000/$DOCKER_IMG_NAME:v$DOCKER_IMG_TAG_VERSION
    [[ $? -ne 0 ]] && printf "Error: Docker Image:$DOCKER_IMG_NAME:v$DOCKER_IMG_TAG_VERSION Creation Failed...exiting\n" && exit 1
}


#Function to list latest image
function list_latest_image_tag()
{
	echo "Latest Image tag is : `curl --silent http://127.0.0.1:5000/v2/helloworld/tags/list | jq .tags[]|sort -r|head -1`"
}

#Function to Install Docker
function install_docker()
{
    printf "Installing Docker...\n"

    apt-get install -y --no-install-recommends \
    linux-image-extra-$(uname -r) \
    linux-image-extra-virtual

    apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

    add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"

    apt-key fingerprintf 0EBFCD88
    apt-get update -y
    apt-get install docker-ce curl jq -y

    [[ $? -ne 0 ]] && printf "Error: Docker Install Failed...exiting\n" && exit 0

    printf "Starting Docker now.."
    systemctl start docker

    [[ $? -ne 0 ]] && printf "Error: Docker Failed to Start...exiting\n" && exit 0

}

function user_mod()
{
	echo "Changing Permission"
	usermod -a -G docker root
	usermod -a -G docker ubuntu
}

install_docker
user_mod
[[ `docker ps|grep registry|wc -l` -eq 0 ]] && start_image_registry
echo "Waiting for Registry"
for version in 1 2
	do
                export DOCKER_IMG_TAG_VERSION=$version
		populate_docker_file
		build_docker_image
        done
list_latest_image_tag
