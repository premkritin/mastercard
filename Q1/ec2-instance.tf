provider "aws" {
  region = "eu-west-2"
}

data template_file "bootstrap" {
  template = file("${path.module}/lib/bootstrap.sh.tpl")
}
resource "aws_instance" "helloworld" {
  ami		= "ami-004c1e61ae5d76090"
  instance_type = "t2.micro"
  user_data     = "${data.template_file.bootstrap.rendered}"
  key_name      = "dockerkey"

  tags = {
    Name = "HelloWorld"
  }
}
